from django.apps import AppConfig


class MadnessappConfig(AppConfig):
    name = 'madnessapp'
