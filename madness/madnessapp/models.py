import uuid
from django.db import models

# Create your models here.


class Team(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, unique=True)
    bracket = models.CharField(max_length=10, default='')
    seed = models.IntegerField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Game(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    home_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='home_team', null=True, default=None,
                                  blank=True)
    away_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='away_team', null=True, default=None,
                                  blank=True)
    home_score = models.IntegerField(null=True, default=None, blank=True)
    away_score = models.IntegerField(null=True, default=None, blank=True)
    round = models.IntegerField()
    game_time = models.DateField()
    is_finished = models.BooleanField()

    # the selection in this game seeds the next game in the later round
    seeds = models.ForeignKey('self', on_delete=models.CASCADE, null=True, default=None, blank=True)

    # used for specifying if this game seeds the home or away team in the seeding team
    seeds_team = models.CharField(max_length=4, default='')

    def __str__(self):
        return '{} at {} in the round of {} and seeds the {} team of {}'.format(self.away_team, self.home_team,
                                                                                self.round, self.seeds_team, self.seeds)




