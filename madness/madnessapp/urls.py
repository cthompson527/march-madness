from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('leagues', views.leagues, name='leagues'),
    path('teams', views.teams, name='teams')
]
