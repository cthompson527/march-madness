from django.shortcuts import render, redirect
from django.urls import reverse
from urllib.parse import urlencode
from .models import Team


def redirect_to_login():
    login = reverse('login')
    query_param = urlencode({'redirected': 'true'})
    redirected_url = '{}?{}'.format(login, query_param)
    return redirect(redirected_url, permanent=False)


def index(request):
    if not request.user.is_authenticated:
        return redirect_to_login()
    return render(request, 'madnessapp/index.html')


def leagues(request):
    if not request.user.is_authenticated:
        return redirect_to_login()
    return render(request, 'madnessapp/leagues.html')


def teams(request):
    if not request.user.is_authenticated:
        return redirect_to_login()
    teams = Team.objects.all()
    return render(request, 'madnessapp/teams.html', {"teams": teams})
